from os.path import split, abspath
import sys
from functools import wraps
from time import sleep

path = split(split(abspath(__file__))[0])[0]
if path not in sys.path:
    sys.path.append(path)

import wxs as wx # just import the library as wx and all works the same
from wxs import Blueprint, url_for, request, redirect
from random import randint
#***************
# this can be read from a database
is_login = True
isadmin = True 
#***************
class counter(object):
    _init = 0
    def increase(self):
        self._init +=1
        return self._init
    def get_value(self):
        return self._init
pos = counter()

miruta = Blueprint('miruta', __file__)

def admin_permision(f):
    @wraps(f)
    def newfunc(*args, **kwds):
        if isadmin:
            return f(*args, **kwds)
        else:
            wx.MessageBox('No es administrador')
    return newfunc

def user_login(f):
    @wraps(f)
    def newfunc(*args, **kwds):
        if is_login:
            return f(*args, **kwds)
        else:
            wx.MessageBox('User is not logged in')
    return newfunc

@miruta.route('printhola')
def printhello():
    print('hello')

@miruta.route('primero', nonblocking=True)
@user_login
@admin_permision
def hola(name, control):
    newval = pos.increase()
    time_sleep = randint(1,40)/10
    sleep(time_sleep)
    texto = 'hola: {}  time: {} pos: {}'.format(name, time_sleep, newval)
    control.SetLabel('\n'.join([control.GetLabel(),texto]))
    return redirect('miruta.printhello')

class My_frame(wx.Frame):
    def __init__(self, parent = None, id=-1, title='Ensayo'):
        super(My_frame, self).__init__(parent, id, title)
        self.pnl = wx.Panel(self)
        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
        st = wx.Button(self.pnl,-1,'Test', name='ensayo')
        self.sizer2.Add(st, 1, wx.EXPAND)
        self.pnl.SetSizer(self.sizer2)
        st.Bind(wx.EVT_BUTTON, url_for('miruta.hola', name=st.Name, control=st))
        st.Bind(wx.EVT_BUTTON, lambda evt: self.callback(evt))
    def callback(self, evt):
        print('--Checking--')
        print(wx.request.GetClassName()) # getting the evt globally
        print(self['ensayo'].Name) # accessing an object by its name
        evt.Skip()
app = wx.App()
frame = My_frame()
ensayo = frame['ensayo'] # accessing the object called ensayo
frame.Show(True)
app.MainLoop()