from __future__ import print_function
import time
from os.path import split, abspath
import sys
from random import randint

path = split(split(abspath(__file__))[0])[0]
if path not in sys.path:
    sys.path.append(path)

import wxs as wx
from wxs import Blueprint, url_for
from rx import Observable

# Define notification event for thread completion
from rx.concurrency import ThreadPoolScheduler

myroute= Blueprint('myroute', __file__)

@myroute.route('Btnpress', nonblocking=True)
def button_press(mainFrame):
    """
    Runs the thread
    """
    Observable.range(1,100).subscribe_on(mainFrame.pool_scheduler).interval(100).subscribe(
        on_next=lambda x: url_for('myroute.updating',mainFrame=mainFrame, message=x)(),
        on_completed=lambda : print("on_completed"),
        on_error=lambda x: print("on_error",x)
    )
    mainFrame['display'].SetLabel("Thread started!")
    mainFrame['btn'].Disable()

@myroute.route('update', nonblocking=True)
def updating(mainFrame, message):
    displayLbl = mainFrame['display']
    test_list= mainFrame['test_list']
    btn = mainFrame['btn']
    t = message
    if isinstance(t, int):
        displayLbl.SetLabel("Time since thread started: %s seconds" % t)
        # ------------->
        # spent time to be connected
        time.sleep(randint(1,4))
        # <--------------
        for i in range(4):
            try:
                test_list.InsertItem(test_list.GetItemCount(), "%s runs" % t)
                break
            except:
                print('An error happend')
    else:
        displayLbl.SetLabel("%s" % t)
        btn.Enable()
class MyForm(wx.Frame):
    # ----------------------------------------------------------------------
    def __init__(self):
        wx.Frame.__init__(self, None, wx.ID_ANY, "Tutorial")
        panel = wx.Panel(self, wx.ID_ANY)
        self.displayLbl = wx.StaticText(panel, label="Amount of time since thread started goes here",
                                        name='display')
        self.btn = btn = wx.Button(panel, label="Start Thread",
                                        name='btn')
        self.test_list = test_list = wx.ListCtrl(panel,
                                        name='test_list',
                                        style=wx.LC_REPORT|wx.BORDER_SUNKEN)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.displayLbl, 0, wx.ALL | wx.CENTER, 5)
        sizer.Add(btn, 0, wx.ALL | wx.CENTER, 5)
        sizer.Add(test_list, 0, wx.ALL | wx.CENTER, 5)
        panel.SetSizer(sizer)
        
        self.test_list.InsertColumn(0,'Test Runs')
        self.test_list.InsertItem(0, "0 test runs")
        
        # Set up event handler for any worker thread results
        self.pool_scheduler = ThreadPoolScheduler(5)
        
        btn.Bind(wx.EVT_BUTTON, url_for('myroute.button_press', mainFrame=self))
# ----------------------------------------------------------------------
# Run the program
if __name__ == "__main__":
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()