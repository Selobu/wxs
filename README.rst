===
WXS
===


**WXS** or simple wxpython is a python library which has the following goals

1. The user can get the last event as a global variable.
2. Simplify the callbacks by using a route method.
3. Decorate your callbacks as much as you need.
4. Allow to transform a function callback into a non blocking callback by using a decorator function.

Installation
============

pip3 install git+https://gitlab.com/Selobu/wxs.git

Easy to use and easy to implement
=================================

You can implement easy wxs to do that you just need to replace wx to wxs

	..  code-block:: python
	
 		import wxs as wx
  
And all your code works the same as wx did

Event as a global variable
--------------------------

 	..  code-block:: python
  
		from wxs import request
		
		...
		
		def callback_function(*args, **kwargs):
		   request.GetIt() # get the current event id


Also it's possible to use it into your classes callbacks

	.. code-block:: python
	
		import wxs as wx
		class Frame(wx.Frame):
		    def __init__(self, *args, **kwargs):
		        .....
		        self.Bind(wx.EVT_BUTTON, self.callback)
		        
		    def callback(self, evt):
		        # the next both methods are equivalent
		        print('Global Id: ', request.GetId()) # request contains the current event
		        print('Local Id: ', evt.GetId())
		        
Doing a callback from another callback is easy
-----------------------------------------------

You just need to know the topic name and the function keywords

  .. code-block:: python
  
  	from wxs import redirect, Blueprint
  	
  	miruta = Blueprint('miruta', __file__)
  	
  	@miruta.route('printhello')
  	def print_something(message):
  		print(message)
  	
  	@miruta.route('test')
  	def callback(*args):
  	    # doing something
  	    redirect('miruta.print_something', message='Printing from a redirection callback')


      
Making a non blocking callback is easy
--------------------------------------

You just need to set the keyword **nonblocking** to True

	.. code-block:: python
	
		from wxs import request, Blueprint, url_for
		import wxs as wx
		
		from random import randint
		from time import sleep
		
		# making a route
		myroute = Blueprint('myroute', __file__)
		
		# setting a listener
		@myroute.route('first', nonblocking=True) # nonblocking callback
		def hello(name, control):
		    time_sleep = randint(1,5)
		    sleep(time_sleep)
		    texto = 'hello: {}  time: {}'.format(name, time_sleep)
		    control.SetLabel('\n'.join([control.GetLabel(),texto]))
		
		class My_frame(wx.Frame):
		    def __init__(self, parent = None, id=-1, title='Ensayo'):
		        super(My_frame, self).__init__(parent, id, title)
		        self.pnl = wx.Panel(self)
		        self.sizer2 = wx.BoxSizer(wx.HORIZONTAL)
		        st = wx.Button(self.pnl,-1,'Test', name='ensayo')
		        self.sizer2.Add(st, 1, wx.EXPAND)
		        self.pnl.SetSizer(self.sizer2)
		        st.Bind(wx.EVT_BUTTON, url_for('myroute.hello', name=st.Name, control=st))
		app = wx.App()
		frame = My_frame()
		frame.Show(True)
		app.MainLoop()

Dependencies
============

* wxpython >= 4.0.1
* python >= 3.5.0

About myself
============

I'm a software professional, I'm living in Colombia with my family. I enjoy to program and I have
a lot of experience under python, c++ and javascript
