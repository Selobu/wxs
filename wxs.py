'''

Copyright (c) 2018 Sebastian Lopez Buritica, selobu at gmail dot com


                          WXS LIBRARY LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>'''

from collections import defaultdict
import wx
from functools import partial, wraps

from threading import Thread
# 
class _threading(Thread):
    """Threatin Class"""
    def __init__(self, func, *args, **kwargs):
        self.func = func
        self.args = args
        self.kwargs = kwargs
        Thread.__init__(self)
        self._return_data = None
        self.start()    # start the thread
    def run(self):
        """Run Worker Thread."""
        self._return_data = self.func( *self.args, **self.kwargs)
    def join(self, timeout=None):
        return self._return_data
        
_blueprint = defaultdict(lambda: defaultdict(list))
_blueprint_locations = dict()

def rendertemplate(xrcname):
    # using jinja2 as the template render
    # checking the route of the current route
    pass

def url_for(listener, **kwargs):
    # translating it
    route, listener, *_ = listener.split('.')
    listener = _blueprint[route][listener]
    def newcall(evt=None):
        return wx.CallAfter(listener, **kwargs) # https://groups.google.com/forum/#!msg/wxpython-users/uJiVmqyaxAg/pUEoGazMA9AJ;context-place=forum/wxpython-users
    return partial(newcall)

def Blueprint(name, __name__, template_folder=''):
    _blueprint[name]
    _blueprint_locations[name] = __name__
    class New:
        @staticmethod
        def route(topic, nonblocking=False, **kwargs):
            def decorator(f):
                if f.__name__ in _blueprint[name]:
                    raise Exception('The function {} is already defined in\n{}'.format(f.__name__, _blueprint_locations[name]))
                @wraps(f)
                def real_decorator(*args, **kwargs):
                    if nonblocking:
                        return _threading(f,*args, **kwargs)
                    else:
                        return f(*args, **kwargs)
                _blueprint[name][f.__name__] = real_decorator
                return real_decorator
            return decorator
    return New
        
def redirect(topic, **kwargs):
    return url_for(topic, **kwargs)(request)

class Request(object):
    @property
    def evt(self):
        return self._evt
    @evt.setter
    def evt(self, evt):
        self._evt = evt
    def __getattr__(self, attr):
        return getattr(self._evt, attr)

request = Request()

class _CtrlBase:
    def __getitem__(self, item_name):
        if isinstance(item_name, str):
            return wx.FindWindowByName(item_name, parent=self)
        elif isinstance(item_name, int):
            return wx.FindWindowById(item_name, parent=self)
        else:
            raise Exception('{}: Unknown item_name it should be '
                            'a Control id or a Control name'.format(self.__name__))
    def Bind(self, *args, **kwargs):
        evtType, callback_func = args[:2]
        if len(args) >2:
            args = args[2:]
        else:
            args = tuple()
        def newcall(evt, *args, **kwargs):
            request.evt = evt
            return callback_func(evt, *args, **kwargs)         
        super(_CtrlBase, self).Bind(evtType, partial(newcall, *args, **kwargs))
        
# wrapping all Controls
for wx_class_name in dir(wx):
    if wx_class_name.startswith('_') \
       or any([(txt in wx_class_name) for txt in ('EVT','NUMPAD','WXK') ]) \
       or wx_class_name == wx_class_name.upper():
        locals()[wx_class_name] = getattr(wx, wx_class_name)
        continue
    try:
        attr = getattr(wx, wx_class_name)
    except:
        continue
    if type(getattr(wx, wx_class_name)) != type(wx.Control):
        locals()[wx_class_name] = attr
        continue
    locals()[wx_class_name] = type(wx_class_name, (_CtrlBase, attr), {'__module__': '__main__', '__doc__': attr.__doc__})

#eof
